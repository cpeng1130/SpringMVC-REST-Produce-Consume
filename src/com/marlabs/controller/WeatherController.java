
package com.marlabs.controller;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.marlabs.model.WeatherInfo;

@Controller
public class WeatherController {
	private static final Logger LOGGER = Logger.getLogger(WeatherInfo.class.getName());

	@RequestMapping("/getWeatherConsume")
	public ModelAndView getWeatherInfo(String zipCode) {
		LOGGER.info(zipCode);
		WeatherInfo weatherInfo = null;
		if (zipCode != null && zipCode.trim().length() != 0) {
			String url = "http://api.openweathermap.org/data/2.5/weather?zip=" + zipCode
					+ ",us&appid=44db6a862fba0b067b1930da0d769e98";
			RestTemplate restTemplate = new RestTemplate();
			weatherInfo = restTemplate.getForObject(url, WeatherInfo.class);
			System.out.println(weatherInfo.getId());
		}
		return new ModelAndView("result", "data", weatherInfo);
	}

	@RequestMapping("/getWeatherProduce")
	@ResponseBody
	public WeatherInfo getWeatherInfo2(String zipCode) {
		LOGGER.info(zipCode);
		WeatherInfo weatherInfo = null;
		if (zipCode != null && zipCode.trim().length() != 0) {
			String url = "http://api.openweathermap.org/data/2.5/weather?zip=" + zipCode
					+ ",us&appid=44db6a862fba0b067b1930da0d769e98";
			RestTemplate restTemplate = new RestTemplate();
			weatherInfo = restTemplate.getForObject(url, WeatherInfo.class);
		}
		return weatherInfo;
	}

	@RequestMapping(value = "/getWeatherProduceXML", produces = "application/xml")
	@ResponseBody
	public WeatherInfo getWeatherInfo3(String zipCode) {
		LOGGER.info(zipCode);
		WeatherInfo weatherInfo = null;
		if (zipCode != null && zipCode.trim().length() != 0) {
			String url = "http://api.openweathermap.org/data/2.5/weather?zip=" + zipCode
					+ ",us&appid=44db6a862fba0b067b1930da0d769e98";
			RestTemplate restTemplate = new RestTemplate();
			weatherInfo = restTemplate.getForObject(url, WeatherInfo.class);
		}
		return weatherInfo;
	}
}
